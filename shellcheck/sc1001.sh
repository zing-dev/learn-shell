#!/usr/bin/env sh

# Want literal backslash
echo Yay \o/

# Want other characters
bell=\a
echo "$bell"
############################################

echo 'Yay \o/'

bell="$(printf '\a')"
echo "$bell"