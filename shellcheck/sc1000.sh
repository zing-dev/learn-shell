#!/usr/bin/env sh

#$ is not used specially and should therefore be escaped.
echo "$"

echo "\$"

#$ is special in double quotes, but there are some cases where it's interpreted literally:
#Following a backslash: echo "\$"
#In a context where the shell can't make sense of it, such as at the end of the string, ("foo$") or before some constructs ("$'foo'").