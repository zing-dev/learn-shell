#!/usr/bin/env sh

set -euxv;

echo "${1#-}"
echo "${1}"
echo "$@"

if [ "${1#-}" != "${1}" ] || [ -z "$(command -v "${1}")" ]; then
  set -- node "$@"
fi

exec "$@"